// let arr = [1, 2, 6, 4, 5, 6];
let arr = [-3, 5, 8, 11, -44];

const iterateAndSum = arr.reduce((sum, number) => {
  return sum + number;
});

console.log(iterateAndSum);
